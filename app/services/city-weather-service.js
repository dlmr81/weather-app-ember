import Service from '@ember/service';
import { inject as service } from '@ember/service';
const WEATHER_API_REQUEST_URL = 'https://api.openweathermap.org/data/2.5/weather?appid=e2e70e88f0db133fc0614acbb938de63&q=';
const API_SUCCESSFUL_CODE = 200
export default Service.extend({
    store: service(),
    init() {
        this._super(...arguments);
    },
    add(cityName) {
        fetch(WEATHER_API_REQUEST_URL + cityName, { method: 'GET' })
            .then((res) => res.json())
            .then(data => {
                if (data.cod === API_SUCCESSFUL_CODE) {
                    let city = this.store.peekRecord('city', data.id);
                    if (city) {
                        city.set('main', data.main);
                        city.set('weather', data.weather);
                    } else {
                        this.store.createRecord('city', data)
                    }
                }
            })

    },
    remove(city) {
        this.store.unloadRecord(city);

    },
    empty() {
        this.store.unloadAll();
    }
});
