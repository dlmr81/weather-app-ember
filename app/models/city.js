import DS from 'ember-data';

export default DS.Model.extend({
   name:DS.attr(),
   main:DS.attr(),
   weather:DS.attr(),
});
