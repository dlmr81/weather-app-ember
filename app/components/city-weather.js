import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    weatherService: service('city-weather-service'),    
    actions:{
        deleteSelf(city){
            this.weatherService.remove(city)
        }
    }
});
