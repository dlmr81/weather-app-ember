import Component from '@ember/component';
import { inject as service } from '@ember/service';
export default Component.extend({
    weatherService: service('city-weather-service'),
    store:service(),
    actions: {
        insertCity() {
            let cityName = this.get('cityName');
            this.weatherService.add(cityName);
            this.set('cityName', '');
            
        },
        clear(){
            this.weatherService.empty();
        },
        deleteCity(city){
            this.weatherService.remove(city)
        }

    }
    ,
});
