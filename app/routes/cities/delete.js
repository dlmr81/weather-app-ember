import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
export default Route.extend({
    weatherService: service('city-weather-service'),
    model(params){
        this.store.peekRecord('city',params.id)
    },
    actions:{
        deleteCity(city){
                this.weatherService.remove(city);
                this.transitionTo('cities');
            }
    }
});
