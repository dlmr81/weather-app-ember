import { helper } from '@ember/component/helper';

export function humidity(humidity) {
  return humidity + ' %';
}

export default helper(humidity);
