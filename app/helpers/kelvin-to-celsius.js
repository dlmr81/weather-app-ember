import { helper } from '@ember/component/helper';

export function kelvinToCelsius(temp) {
  return (temp-273.14).toFixed(2) + ' C';
}

export default helper(kelvinToCelsius);
