import { helper } from '@ember/component/helper';

export function pressure(pressure) {
  return pressure +" hPa";
}

export default helper(pressure);
